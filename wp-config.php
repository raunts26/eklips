<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'eklips');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'vh@_VROg)>0x,Y&{6Lffn&Z%Y%mu`l5$m4d+I^y&qcY1XrRh0#fu4j*xHK_*FT,?');
define('SECURE_AUTH_KEY',  '53r?Ky2|^$/?YIyYqUZLWHm!@/2>pm)`,^K3L8O>>!y0]#=0h$~#rDdM )>dwZ`K');
define('LOGGED_IN_KEY',    '%O8ZN].?3ug{=t|)CLE|RTN?{T3UTBggy1fP<A/Dx;jZfvP[w)^t(kd.nn%?.Cd?');
define('NONCE_KEY',        'D5j-cDt$-J#J]Gpw-C;C6K,$xkui=Z?xe# >#& !e)H:WXwc;mJ;w{uE8:;e<H]!');
define('AUTH_SALT',        '>fXt`aF[zM,JI-4IJ~ki@J%)f[dB0du-1`Y)#Og*_F<4%j%]q!p--f~ KHXx_Egs');
define('SECURE_AUTH_SALT', 'Zj7wyhjZ8Tq].<.uzgjl}0P#PW1t}bG:s@`Bcg([c.w9v^(?XBNG}B.yOl>|U6^E');
define('LOGGED_IN_SALT',   '>[(&9Kd.|LCKCXzi&^hu~KlKcDE),62(7tEE30$x_ag)1/)|)!]atV@hg<s9,AF[');
define('NONCE_SALT',       'o*}t0% <i0AL^wb{Rwirp^$ctk[2_8_qS :ba9A_K1ESKJX-pbU$I~8+7DI1D-W2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
